import './App.css';
import { BrowserRouter,Routes, Route } from 'react-router-dom';
import Billing from './pages/Billing';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Billing/>}/>
          <Route path='/order' element={<Billing/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
