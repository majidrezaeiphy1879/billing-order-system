import React from "react";
import { IoIosCheckmark } from "react-icons/io";
import { BsX } from "react-icons/bs";
import gTick from '../images/icons8-tick-90.png' 
import redx from '../images/icons8-x-96(1).png'
function Table() {
  return (
    <div style={{ paddingTop: "2rem" }}>
      <table style={{width:'100%'}}>
        <tr className="tr-even-style" >
          <td className="td-title">Developer</td>
          <td className="td-style">Up to 1</td>
          <td className="td-style">Up to 10</td>
          <td className="td-style">Up to 10</td>
        </tr>
        <tr >
          <td className="td-title">Number of products</td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr className="tr-even-style">
          <td className="td-title">Internal Apps</td>
          <td className="td-style"><img src={redx} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr>
          <td className="td-title">Saas Apss</td>
          <td className="td-style"><img src={redx} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={redx} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr className="tr-even-style">
          <td className="td-title">Software Update</td>
          <td className="td-style">Always Latest Version</td>
          <td className="td-style">Always Latest Version</td>
          <td className="td-style">Always Latest Version</td>
        </tr>
        <tr>
          <td className="td-title">Techinical Support</td>
          <td className="td-style">Community</td>
          <td className="td-style">Professional</td>
          <td className="td-style">Professional</td>
        </tr>
        <tr  className="tr-even-style">
          <td className="td-title">Full Sources Code</td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr>
          <td className="td-title">Domen Grid</td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr className="tr-even-style">
          <td className="td-title">Domen Chart</td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr>
          <td className="td-title">Domen Widget</td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
        <tr className="tr-even-style">
          <td className="td-title">Domen Export</td>
          <td className="td-style"><img src={redx} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
          <td className="td-style"><img src={gTick} style={{height:'25px'}} alt="" /></td>
        </tr>
      </table>
    </div>
  );
}

export default Table;
