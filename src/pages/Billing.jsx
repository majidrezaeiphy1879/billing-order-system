import React, { useState } from "react";
import Table from "../components/Table";

let servicePrice = [45, 80, 105];

function Billing() {
  const [annualBilling, setAnnualBilling] = useState(false);

  const handleToggleBilling = () => {
    setAnnualBilling(!annualBilling);
  };

  const getPriceWithDiscount = (index) => {
    const discountFactor = annualBilling ? 0.72 : 1; // 28% discount for annual billing
    return `$${Math.round(servicePrice[index] * discountFactor)}`;
  };

  const priceItems = servicePrice.map((price, index) => {
    return {
      label: index === 0 ? "Basic" : index === 1 ? "Pro" : "Enterprise",
      price: getPriceWithDiscount(index),
      content:
        index === 0
          ? "Prefect for small internal app or dashboard"
          : index === 1
          ? "Prefect for Saas Apps with small team"
          : "Prefect for Saas Apps with large teams and require on prems",
      discount: annualBilling,
    };
  });

  const terrifItems = priceItems.map((item, index) => {
    return (
      <div
        key={index}
      >
        <div>
          <h5 className="items-label">{item.label}</h5>
          <h3 className="h3-price">
            {item.price}
            <span style={{ fontSize: "23px", color: " #cad1df " }}>/Mo</span>
          </h3>
          <p
            style={{
              marginBottom: "1rem",
              paddingLeft: "10px",
              width: "90%",
              paddingRight: "10px",
            }}
          >
            {item.content}
          </p>
          <a className="btn-style" href="#">
            Choose
          </a>
        </div>
      </div>
    );
  });

  return (
    <div className="container-div">
      <div></div>
      <div className="toggle-div">
        <p
          className="bill-monthly"
          style={{
            backgroundColor: annualBilling ? "" : "rgb(75, 190, 75)",
          }}
          onClick={handleToggleBilling}
        >
          Bill monthly
        </p>
        <p
          onClick={handleToggleBilling}
          className="bill-monthly"
          style={{ backgroundColor: annualBilling ? "rgb(75, 190, 75)" : "" }}
        >
          Bill annually Save 28%
        </p>
      </div>
      <div className="terrif-div">
        <div className="terrif-top">
          <img
            src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffutureentech.com%2Fstorage%2F2019%2F09%2Fbig-data-analyst.jpg&f=1&nofb=1&ipt=b8ab530ff1d11cf70cd928ee8f1ee27ffb796f6679c997d5be76dbbacac8ead3&ipo=images"
            alt=""
            style={{
              height: "250px",
              width: "250px",
              borderRadius: "10px",
            }}
          />
          <div style={{display:'flex',justifyContent:'center',textAlign:'center'}}>
            <div className="popular-plan-tag">Popular Plan</div>
            {terrifItems}
          </div>
        </div>
        <Table />
      </div>
    </div>
  );
}

export default Billing;
